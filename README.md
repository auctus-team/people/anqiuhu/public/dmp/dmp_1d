# Model of the Dynamical Movement Primitives(DMP):
Nonlinear attractor learnable system:

$\tau \dot{z} = a_y(b_y(g-y)-z) + f(x)$

$\tau \dot{y} = z$,

$\tau \dot{x} = \alpha_x x$

Where $x$ is the phase variable, $y$ is the system state,parameters $a_y$ and $b_y$ define the behavior of the second order system, $\tau$ is a time constant, $g$ is the goal.

$f$ is the forcing term, which is defined as a linear combination of N nonlinear Radial Basis Functions (RBFs), which enables the robot to follow any smooth trajectory from the initial position $y_0$ to the final configuration $g$

$f(x) = \frac{\sum_{i=1}^Nw_i\Psi_i(x)}{\sum_{i=1}^N\Psi_i(x)}x(g-y_0)$

$\Psi_i(x) = \exp(-h_i(x-c_i)^2)$ 

where $c_i$ are the centers of Gaussian basis functions distributed along the phase of the movement and $h_i$ their widths


Without forcing time, the equation represent a globally stable second-order linear system with (z, y) = (0, g) as a unique point attractor.

⇒
$\tau^2 \ddot{y} = a_y(b_y(g-y)-\tau \dot{y})$

damping ratio $\xi=\frac{1}{2}\sqrt{\frac{a_y}{b_y}}$







# System1: 
$a_y=24, b_y=a_y/4=6$,  damping ratio $\xi=1$, Critically damped
<figure>
<img src="pictures/system ay=24,by=6/system_tau=0.5.png" alt="$\tau=0.5$" width="33%">
<img src="pictures/system ay=24,by=6/system_tau=1.png" alt="$\tau=1$" width="33%">
<img src="pictures/system ay=24,by=6/system_tau=1.5.png" alt="$\tau=1.5$" width="33%">
</figure>

 * With a smaller time constant, the system response and converge faster.

## desired path:  $sin(x)/(sin(x)[-1])$
$\tau=0.5$:
![image info](pictures/system ay=24,by=6/path1_tau=0.5.png)
$\tau=1.0$:
![image info](pictures/system ay=24,by=6/path1_tau=1.png)
$\tau=1.5$:
![image info](pictures/system ay=24,by=6/path1_tau=1.5.png)



## desired path:  a step function
<figure>
<img src="pictures/system ay=24,by=6/path2_tau=0.8.png" alt="$\tau=0.8$" width="33%">
<img src="pictures/system ay=24,by=6/path2.png" alt="$\tau=1$" width="33%">
<img src="pictures/system ay=24,by=6/path2_tau=1.2.png" alt="$\tau=1.2$" width="33%">
</figure>

*  A larger time constant leads to a slower convergence, which might be useful for smooth.
*  A lower time constant enhances the system’s ability to adapt to sudden changes.

<!--## desired path: $3x^2 - 2x^3$
![image info](pictures/system ay=24,by=6/path3.png)-->

# System2
 $a_y=24, b_y=a_y=24$, damping ratio $\xi=0.5$, Uderdamped

 Overshot $M_p=e^{\frac{-\xi\pi}{\sqrt(1-\xi^2)}}$
<figure>
<img src="pictures/system ay=24,by=24/system_tau=0.5.png" alt="$\tau=0.5$" width="33%">
<img src="pictures/system ay=24,by=24/system_tau=1.png" alt="$\tau=1$" width="33%">
<img src="pictures/system ay=24,by=24/system_tau=1.5.png" alt="$\tau=1.5$" width="33%">
</figure>

 * The system exhibits oscillatory behavior, with the amplitude of oscillations gradually decreasing over time.
 * It has a relatively quick initial response but takes longer to settle due to the oscillations.

## desired path:  $sin(x)/(sin(x)[-1])$
$\tau=0.5$:
![image info](pictures/system ay=24,by=24/path1_tau=0.5.png)
$\tau=1.0$:
![image info](pictures/system ay=24,by=24/path1_tau=1.png)
$\tau=1.5$:
![image info](pictures/system ay=24,by=24/path1_tau=1.5.png)

* A larger time constant leads to a slower convergence, which might be useful for smooth.

## desired path:  a step function
![image info](pictures/system ay=24,by=24/path2.png)
<!--## desired path: $3x^2 - 2x^3$
![image info](pictures/system ay=24,by=24/path3.png)-->

# System3: 
$a_y=27, b_y=a_y/9=3$, damping ratio $\xi=1.5$, Overdamped
<figure>
<img src="pictures/system ay=27,by=3/system_tau=0.5.png" alt="$\tau=0.5$" width="33%">
<img src="pictures/system ay=27,by=3/system_tau=1.png" alt="$\tau=1$" width="33%">
<img src="pictures/system ay=27,by=3/system_tau=1.5.png" alt="$\tau=1.5$" width="33%">
</figure>

## desired path:  $sin(x)/(sin(x)[-1])$
$\tau=0.5$:
![image info](pictures/system ay=27,by=3/path1_tau=0.5.png)
$\tau=1.0$:
![image info](pictures/system ay=27,by=3/path1_tau=1.png)
$\tau=1.5$:
![image info](pictures/system ay=27,by=3/path1_tau=1.5.png)

## desired path:  a step function
![image info](pictures/system ay=27,by=3/path2.png)
<!--## desired path: $3x^2 - 2x^3$
![image info](pictures/system ay=27,by=3/path3.png)-->

# Conclusion:
1. The time constant determines how quickly the system can adapt to reach the target state or follow the desired trajectory. A smaller time constant results in a faster convergence rate.
2.  A larger time constant leads to a slower convergence, which might be useful for smooth.
3. A lower time constant enhances the system’s ability to adapt to sudden changes.